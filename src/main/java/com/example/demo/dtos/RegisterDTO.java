package com.example.demo.dtos;

import lombok.Data;

@Data
public class RegisterDTO {
    private String login;
    private String password;
    private String name;
}
