package com.example.demo.dtos;

import lombok.Data;

@Data
public class MessagesDTO {

    private Long userId;
    private Long chatId;
    private String text;
}
