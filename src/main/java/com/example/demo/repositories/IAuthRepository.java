package com.example.demo.repositories;

import com.example.demo.models.Auth;
import com.example.demo.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository

public interface IAuthRepository extends JpaRepository<Auth,Long> {

    Auth getByLogin(String login);
    Boolean existsByToken(String token);

}
