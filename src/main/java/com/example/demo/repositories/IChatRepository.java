package com.example.demo.repositories;

import com.example.demo.models.Chat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IChatRepository extends JpaRepository<Chat, Long> {

    Chat getById(Long id);

    boolean existsById(Long id);
}
