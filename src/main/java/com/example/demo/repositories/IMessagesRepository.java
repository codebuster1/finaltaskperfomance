package com.example.demo.repositories;

import com.example.demo.models.Messages;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IMessagesRepository extends JpaRepository<Messages, Long> {

    Boolean existsByChatId(Long id);

    List<Messages> findByChatId(Long id);

    boolean existsById(Long id);

    Optional<Messages> findById(Long id);

}
