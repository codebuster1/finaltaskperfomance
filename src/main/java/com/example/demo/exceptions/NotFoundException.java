package com.example.demo.exceptions;

import java.util.function.Supplier;

public class NotFoundException extends Exception {

    public NotFoundException() {
    }

    public NotFoundException(String message) {
        super(message);
    }
}
