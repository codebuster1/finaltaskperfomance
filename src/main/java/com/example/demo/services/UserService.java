package com.example.demo.services;

import com.example.demo.dtos.UserDTO;
import com.example.demo.exceptions.BadRequestException;
import com.example.demo.exceptions.NotFoundException;
import com.example.demo.models.User;
import com.example.demo.repositories.IAuthRepository;
import com.example.demo.repositories.IMessagesRepository;
import com.example.demo.repositories.IUserRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor

public class UserService {

    private final IUserRepository userRepository;
    private final IAuthRepository authRepository;

   /* @Autowired
    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }
*/
    public User getUser(Long id, String token) throws NotFoundException, BadRequestException {

        if (!authRepository.existsByToken(token)){
            throw new BadRequestException("INCORRECT TOKEN");
        }

        if (id == null){
            throw new BadRequestException("id is null");
        }
        Optional<User> optionalUser = userRepository.findById(id);

        if (optionalUser.isPresent()){
            return optionalUser.get();
        }else {
            throw new NotFoundException("This login not exist");
        }
    }

    private User getUser (Long id) throws BadRequestException, NotFoundException {
        if (id == null){
            throw new BadRequestException("id is null");
        }
        Optional<User> optionalUser = userRepository.findById(id);

        if (optionalUser.isPresent()){
            return optionalUser.get();
        }else {
            throw new NotFoundException("This login not exist");
        }
    }

    public User addUser(User user) throws BadRequestException {

        if (user.getName() == null || user.getName().isBlank()){
            throw new BadRequestException("name is null");
        }

        User newUser = User.builder()
                .name(user.getName())
                .build();
        return userRepository.save(newUser);
    }


    public User deleteUser(Long id, String token) throws NotFoundException, BadRequestException {

        if (!authRepository.existsByToken(token)){
            throw new BadRequestException("TOKEN ISNT EXIST");
        }

        User user = getUser(id);
        userRepository.delete(user);
        return user;
    }

    public User updateUser(User user, String token) throws NotFoundException, BadRequestException {

        if (!authRepository.existsByToken(token)) {
            throw new BadRequestException("ERROR : TOKEN is INCORRECT");
        }
        User updateUser = getUser(user.getId());

        if (user.getName() != null){
            updateUser.setName(user.getName());
        }

        return userRepository.save(updateUser);
    }

}
