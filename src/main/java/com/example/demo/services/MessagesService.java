package com.example.demo.services;

import com.example.demo.dtos.MessagesDTO;
import com.example.demo.exceptions.BadRequestException;
import com.example.demo.exceptions.NotFoundException;
import com.example.demo.models.Messages;
import com.example.demo.models.User;
import com.example.demo.repositories.IAuthRepository;
import com.example.demo.repositories.IMessagesRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class MessagesService {

    private final IMessagesRepository messagesRepository;
    private final IAuthRepository authRepository;

  /*  @Autowired
    public MessagesService(IMessagesRepository messagesRepository) {
        this.messagesRepository = messagesRepository;
    }
*/
    public List<Messages> getMessagesByChatId(Long chatId,String token) throws Exception {

        if (!authRepository.existsByToken(token)) {
            throw new Exception("ERROR : TOKEN is INCORRECT");
        }

        if (chatId == null){
            throw new BadRequestException("chatId is null");
        }

        if (!messagesRepository.existsByChatId(chatId)){
            throw new BadRequestException("chat id is not exist");
        }

        return messagesRepository.findByChatId(chatId);
    }

    public Messages addMessage(MessagesDTO message,String token) throws BadRequestException {

        if (!authRepository.existsByToken(token)) {
            throw new BadRequestException("INCORRECT TOKEN") ;
        }

        if (message.getChatId() == null){
            throw new BadRequestException("chat id is null");
        }
        if (message.getUserId() == null){
            throw new BadRequestException("user id is null");
        }
        if (message.getText() == null){
            throw new BadRequestException("text id is null");
        }

        Messages messages = Messages.builder()
                .chatId(message.getChatId())
                .userId(message.getUserId())
                .text(message.getText())
                .build();

        return  messagesRepository.save(messages);
    }

    public String deleteById(Long messageId,String token) throws BadRequestException {

        if (authRepository.existsByToken(token)) {
            throw new BadRequestException("ERROR : TOKEN is INCORRECT");
        }

        if (messageId == null){
            throw new BadRequestException("message id is null");
        }

        if (!messagesRepository.existsById(messageId)){
            throw new BadRequestException("such message id is not exist");
        }

        messagesRepository.deleteById(messageId);
        return "Succesfull deleted";
    }

    public Messages update(Messages messages, String token) throws NotFoundException, BadRequestException {

        if (!authRepository.existsByToken(token)) {
            throw new BadRequestException("ERROR : TOKEN is INCORRECT");
        }
            Optional<Messages> updateMessage = messagesRepository.findById(messages.getId());

        if (updateMessage.isPresent()){
            updateMessage.get().setChatId(messages.getChatId());
            updateMessage.get().setText(messages.getText());
            updateMessage.get().setUserId(messages.getUserId());
            return messagesRepository.save(updateMessage.get());
        }else {
            throw new NotFoundException("This message not exist");
        }

    }

}
