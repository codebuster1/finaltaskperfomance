package com.example.demo.services;

import com.example.demo.exceptions.BadRequestException;
import com.example.demo.models.Chat;
import com.example.demo.repositories.IAuthRepository;
import com.example.demo.repositories.IChatRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ChatService {

    private final IChatRepository chatRepository;
    private final IAuthRepository authRepository;

    /*@Autowired
    public ChatService(IChatRepository chatRepository) {
        this.chatRepository = chatRepository;
    }
*/
    public Chat getChatById(Long id) throws BadRequestException {
        if (chatRepository.existsById(id)) {
            return chatRepository.getById(id);
        }else {
            throw new BadRequestException("such id is not exist");
        }
    }

    public Chat createChat(Chat chat,String token) throws Exception {

        if (authRepository.existsByToken(token)) {
            return chatRepository.save(chat);
        }else {
            throw new Exception("ERROR : TOKEN is INCORRECT");
        }
    }

    public Chat updateChat(Chat chat, String token) throws BadRequestException {

        if (!chatRepository.existsById(chat.getId())){
            throw new BadRequestException("such chat not exist");
        }

        if (!authRepository.existsByToken(token)) {
            throw new BadRequestException("ERROR : INCORRECT token");
        }

        if (chat.getId() != null){
            throw new BadRequestException("id is null");
        }

        if (chat.getName() != null){
            throw new BadRequestException("name is null");
        }

        Chat updatedChat = chatRepository.getById(chat.getId());
        updatedChat.setName(chat.getName());
        updatedChat.setId(chat.getId());

        return chatRepository.save(updatedChat);

    }

    public String deleteChatById(Long id, String token) throws Exception {

        if (id != null){
            throw new BadRequestException("id is null");
        }

        if (authRepository.existsByToken(token)) {
            chatRepository.deleteById(id);
            return "Succesfully deleted";
        }else {
            throw new Exception("ERROR : TOKEN is INCORRECT");
        }

    }

}
