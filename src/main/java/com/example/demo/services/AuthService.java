package com.example.demo.services;

import com.example.demo.dtos.AuthDTO;
import com.example.demo.dtos.RegisterDTO;
import com.example.demo.exceptions.BadRequestException;
import com.example.demo.models.Auth;
import com.example.demo.models.User;
import com.example.demo.repositories.IAuthRepository;
import com.example.demo.repositories.IUserRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.UUID;


@Service
@AllArgsConstructor

public class AuthService {

    private final IAuthRepository authRepository;
    private final IUserRepository userRepository;

   /* @Autowired
    public AuthService(IAuthRepository authRepository) {
        this.authRepository = authRepository;
    }
*/

    public Auth login(AuthDTO user) throws Exception {

        Auth userFromDB = authRepository.getByLogin(user.getLogin());

        if (userFromDB != null){
            if(userFromDB.getPassword().equals(user.getPassword())){
                String token = generateToken(user);
                userFromDB.setToken(token);
                userFromDB.setUserId(userFromDB.getUserId());
            }else {
                throw new BadRequestException("ERROR : INCORRECT PASSWORD");
            }
        }else {
            throw new BadRequestException("ERROR : SUCH USER DOESNT EXIST");
        }

        authRepository.save(userFromDB);
        return userFromDB;
    }

    public String register(RegisterDTO user) throws Exception {

        if (user.getPassword() != null && user.getLogin() != null ){

            User newPerson = new User();
            newPerson.setName(user.getName());
            userRepository.save(newPerson);

            Auth newUser = new Auth();
            newUser.setLogin(user.getLogin());
            newUser.setPassword(user.getPassword());

            newUser.setUserId(newPerson.getId());
            authRepository.save(newUser);
            return "Succesfully created";
        }else {
            throw  new Exception("Your password or login null");
        }
    }

    public String deleteAuth(AuthDTO user) throws BadRequestException {

        if (user == null ){
            throw new BadRequestException("user is null");
        }

        if (user.getLogin() == null || user.getLogin().isBlank()){
            throw new BadRequestException("login is null");
        }

        if (user.getPassword() == null || user.getPassword().isBlank()){
            throw new BadRequestException("password is null");
        }

        Auth deleteUser = new Auth();
        deleteUser = authRepository.getByLogin(user.getLogin());
        authRepository.deleteById(deleteUser.getUserId());
        return "Successfully deleted";
    }

    public Auth updateAuth(AuthDTO user) throws BadRequestException {

        if (user == null ){
            throw new BadRequestException("user is null");
        }

        if (user.getLogin() == null || user.getLogin().isBlank()){
            throw new BadRequestException("login is null");
        }

        if (user.getPassword() == null || user.getPassword().isBlank()){
            throw new BadRequestException("password is null");
        }

        Auth updateUser = new Auth();
        updateUser = authRepository.getByLogin(user.getLogin());
        updateUser.setLogin(user.getLogin());
        updateUser.setPassword(user.getPassword());

        return updateUser;
    }

    private String generateToken(AuthDTO user){
        Calendar cal = Calendar.getInstance();
        String token = UUID.randomUUID().toString().toUpperCase()
                +"-" + user.getPassword().getBytes()+"-"
                +cal.getTimeInMillis();
        return token;
    }

}
