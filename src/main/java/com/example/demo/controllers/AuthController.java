package com.example.demo.controllers;

import com.example.demo.dtos.AuthDTO;
import com.example.demo.dtos.RegisterDTO;
import com.example.demo.exceptions.BadRequestException;
import com.example.demo.models.Auth;
import com.example.demo.models.Chat;
import com.example.demo.services.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/auth")

public class AuthController {
    private AuthService authService;

    @PostMapping("/auth")
    public ResponseEntity<?> authUser(@RequestBody AuthDTO user) throws Exception {
        return ResponseEntity.ok(authService.login(user));
    }

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody RegisterDTO user )throws  Exception{
        return ResponseEntity.ok(authService.register(user));
    }

    @PutMapping("/update")
    public ResponseEntity<?> updateAuthDate(@RequestBody AuthDTO user) throws BadRequestException {
        return ResponseEntity.ok(authService.updateAuth(user));
    }

    @DeleteMapping("/delete")
    public ResponseEntity<?> delete(@RequestBody AuthDTO user) throws BadRequestException {
        return ResponseEntity.ok(authService.deleteAuth(user));
    }
}
