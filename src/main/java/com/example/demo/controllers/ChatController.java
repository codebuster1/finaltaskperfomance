package com.example.demo.controllers;

import com.example.demo.exceptions.BadRequestException;
import com.example.demo.models.Chat;
import com.example.demo.repositories.IAuthRepository;
import com.example.demo.services.AuthService;
import com.example.demo.services.ChatService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/chat")

public class ChatController {

    private ChatService chatService;

    @GetMapping("/getById/{id}")
    public ResponseEntity<?> getByChatId(@PathVariable Long id) throws BadRequestException {
        return ResponseEntity.ok( chatService.getChatById(id));
    }

    @PostMapping("")
    public ResponseEntity<?> addChat(@RequestBody Chat chat, @RequestHeader String token) throws Exception {
        return  ResponseEntity.ok(chatService.createChat(chat, token));
    }

    @DeleteMapping("/{id}")
    public  ResponseEntity<?> deleteChatById(@PathVariable Long id , @RequestHeader String token) throws Exception {
        return ResponseEntity.ok(chatService.deleteChatById(id, token));
    }

    @PutMapping("")
    public  ResponseEntity<?> updateById(@RequestBody Chat chat, @RequestHeader String token) throws Exception {
        return ResponseEntity.ok(chatService.updateChat(chat, token));
    }

}
