package com.example.demo.controllers;

import com.example.demo.models.User;
import com.example.demo.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/user")

public class UsersController {

    private UserService userService;

    @GetMapping("/getUserById/{id}")
    public ResponseEntity<?> getAll(@RequestHeader String token, @PathVariable Long id) throws Exception {
        return ResponseEntity.ok(userService.getUser(id,token));
    }

    @PostMapping("/create")
    public ResponseEntity<?> newUser(@RequestBody User user) throws Exception {
        return ResponseEntity.ok(userService.addUser(user));
    }

    @PutMapping("/update")
    public ResponseEntity<?> updateUser(@RequestBody User user, @RequestHeader String token) throws Exception {

        return ResponseEntity.ok(userService.updateUser(user,token));
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteUserById(@PathVariable Long id, @RequestHeader String token) throws Exception {
        return ResponseEntity.ok(userService.deleteUser(id,token));
    }

}
