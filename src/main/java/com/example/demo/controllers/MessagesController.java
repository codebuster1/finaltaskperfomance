package com.example.demo.controllers;

import com.example.demo.dtos.MessagesDTO;
import com.example.demo.models.Messages;
import com.example.demo.services.MessagesService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/message")

public class MessagesController {
    private  final MessagesService messagesService;

    @GetMapping("/chat/{id}")
    public ResponseEntity<?> getMessagesByChatId(@PathVariable Long id , @RequestHeader String token) throws Exception {
        return ResponseEntity.ok(messagesService.getMessagesByChatId(id,token));
    }

    @PostMapping("/add")
    public ResponseEntity<?> addMessage(@RequestBody MessagesDTO message, @RequestHeader String token) throws Exception {
        return ResponseEntity.status(HttpStatus.CREATED).body(messagesService.addMessage(message,token));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteMessageById(@PathVariable Long id, @RequestHeader String token) throws Exception {

        return ResponseEntity.ok(messagesService.deleteById(id,token));
    }

    @PutMapping("/updateById/{}")
    public ResponseEntity<?> updateMessageById(@RequestBody Messages message, @RequestHeader String token) throws Exception {
        return ResponseEntity.ok(messagesService.update(message, token));
    }

}
